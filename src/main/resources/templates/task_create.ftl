<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Task creating</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container col-5 text-center ">
    <br>
    <form class="form-horizontal " action="/add" method="POST">
            <div class="card indigo form-white">
                <div class="card-body">
                    <h3 class="text-center "><i class="fa fa-th-list"></i> To Do</h3>
                    <div class="md-form">
                        <label for="name">Name of task</label>
                        <input name="name" type="text" id="name" class="form-control" required="true">
                    </div>

                    <div class="md-form">
                        <br>
                        <label for="description">Description</label>
                        <textarea name="description" type="text" id="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <br>
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                            <option>TO_DO</option>
                            <option>IN_PROGRESS</option>
                            <option>DONE</option>
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input type="submit" class="btn btn-primary btn-lg " value="  Save  "/>
                    </div>
                </div>
            </div>
        </div>
</div>
</form>
</div>
</body>
</html>