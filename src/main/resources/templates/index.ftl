<#import "/spring.ftl" as spring/>

<!DOCTYPE HTML>
<html>
<head>
        <meta charset="utf-8">
        <title>To Do board</title>
    <style>
        body { background: url(background.jpg); }
    </style>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>

<body>
<#include "navbar.ftl">
<div class="container-fluid">
    <div class="row">
        <div class="col-md">
            <br>
            <h3 class="text-center text-primary">To Do</h3>
            <div class="table">
                <table class="table table-striped table-bordered text-left">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class= "table-primary">
               <#list taskList as task>
               <#assign status = task.status>
       <#if status=="TO_DO">
                    <tr>
                        <td>${task.name}</td>
                        <td><textarea type="text" class="form-control" readonly>${task.description}</textarea>
                        </td>
                        <td>
                            <a href="/edit/${task.id}"><span
                                    class="fa fa-pencil text-center" style="font-size:18px"></span> </a></td>
                           <td> <a href="/delete/${task.id}"><span
                            class="fa fa-trash text-center" style="font-size:18px"></span> </a></td>
                        </td>
                    </tr>
          </#if>
                 </#list>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md">
            <br>
            <h3 class="text-center text-warning">In Progress</h3>
            <div class="table">
                <table class="table table-striped table-bordered text-left">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class= "table-warning">
                     <#list taskList as task>
                         <#assign status = task.status>
                         <#if status =="IN_PROGRESS">
                    <tr>
                        <td>${task.name}</td>
                        <td><textarea type="text" class="form-control" readonly>${task.description}</textarea></td>
                        <td>
                            <a href="/edit/${task.id}"><span
                                    class="fa fa-pencil text-center" style="font-size:18px"></span></a></td>
                        <td><a href="/delete/${task.id}"><span
                                class="fa fa-trash text-center" style="font-size:18px"></span></a></td>
                        </td>
                         </#if>
                     </#list>
                    </tr>
                    </tbody>
                </table>
        </div>
        </div>
        <div class="col-md">
            <br>
            <h3 class="text-center text-success">Done</h3>
            <div class="table">
                <table class="table table-striped table-bordered text-left">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class= "table-success">
                    <#list taskList as task>
                    <#assign status = task.status>
                         <#if status == "DONE">
                    <tr>
                        <td>${task.name}</td>
                        <td><textarea type="text" class="form-control" readonly>${task.description}</textarea></td>
                        <td>
                            <a href="/edit/${task.id}"><span
                                    class="fa fa-pencil text-center" style="font-size:18px"></span></a></td>
                            <td><a href="/delete/${task.id}"><span
                                    class="fa fa-trash text-center" style="font-size:18px"></span></a></td>
                    </tr>
                         </#if>
                    </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>