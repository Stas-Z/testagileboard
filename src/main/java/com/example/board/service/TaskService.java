package com.example.board.service;

import java.util.List;
import java.util.Optional;

public interface TaskService <T ,D> {

    List<T> getAll();

    Optional<T> getById(final String entityId);

    boolean insert(final D entity);

    boolean delete(final String entityId);

    boolean edit(final D entity);
}
