package com.example.board.service;

import java.util.List;
import java.util.Optional;

import com.example.board.entity.Task;
import com.example.board.entity.TaskStatus;
import com.example.board.dto.TaskDto;
import com.example.board.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService <Task, TaskDto> {

    private final transient TaskRepository repository;

    @Autowired
    public TaskServiceImpl (final TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Task> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Task> getById (final String entityId) {
        return repository.findById(entityId);
    }

    @Override
    public boolean insert (final TaskDto entity) {
        repository.insert(parseEntity(entity));
        return true;
    }

    @Override
    public boolean delete (final String entityId) {
        repository.deleteById(entityId);
        return true;
    }

    @Override
    public boolean edit (final TaskDto entity) {
        repository.save(parseEntity(entity));
            return true;
    }

    private Task parseEntity (final TaskDto taskDto) {
        final Task task = new Task();
        if (taskDto.getId() != null) {
            task.setId(taskDto.getId());
        }
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setStatus(TaskStatus.valueOf(taskDto.getStatus()));

        return task;
    }
}
