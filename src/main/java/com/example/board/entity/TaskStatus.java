package com.example.board.entity;

public enum  TaskStatus {
    TO_DO, IN_PROGRESS, DONE
}
