package com.example.board.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private String id;

    private String name;
    private String description;
    private String status;

    public TaskDto(String name, String description, String status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
