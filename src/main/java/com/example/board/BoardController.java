package com.example.board;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.ui.Model;
import com.example.board.entity.TaskStatus;
import com.example.board.dto.TaskDto;
import com.example.board.service.TaskServiceImpl;

@Controller
public class BoardController {

    @Autowired
    private transient TaskServiceImpl service;

    private static final String REDIRECT_PATH = "redirect:/";

    @GetMapping("/")
    public String home(final Model model) {
        model.addAttribute("taskList", service.getAll());
        return "index";
    }

    @GetMapping("/add")
    public String add() {
        return "task_create";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute("task") final TaskDto task, final Model model) {
        service.insert(task);
        return REDIRECT_PATH;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") final String id) {
        service.delete(id);
        return REDIRECT_PATH;
    }

    @GetMapping("/edit/{id}")
    public String update(@PathVariable("id") final String id, final Model model) {
        model.addAttribute("task", service.getById(id).get());
        return "task_edit";
    }

    @PostMapping("/edit")
    public String update(@ModelAttribute("task")  final TaskDto task) {
        service.edit(task);
        return REDIRECT_PATH;
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}

